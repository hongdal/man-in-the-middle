#!/bin/bash


wget http://openvswitch.org/releases/openvswitch-2.5.4.tar.gz
tar -xf openvswitch-2.5.4.tar.gz
cd openvswitch-2.5.4/
./configure --with-linux=/lib/modules/`uname -r`/build
make 
make install
make modules_install



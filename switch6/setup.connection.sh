#!/bin/bash

if [[ -z $1 ]]; then 
    echo " Usage: $0 <controller-ip>" 
    exit 1
fi

ovs-vsctl add-br switch6
ovs-vsctl set-controller switch6 tcp:$1:6653

ifconfig eth1 0
ifconfig eth2 0
ifconfig switch6 10.130.127.2 netmask 255.255.255.0 up 

ovs-vsctl add-port switch6 eth1
ovs-vsctl add-port switch6 eth2



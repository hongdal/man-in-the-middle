#!/bin/bash

apt-get update
apt-get install software-properties-common -y 
add-apt-repository ppa:webupd8team/java -y
apt-get update
apt-get install oracle-java8-set-default -y


apt-get install build-essential ant maven python-dev eclipse -y

git clone git://github.com/floodlight/floodlight.git
cd floodlight  

git submodule init 
git submodule update

ant 

mkdir /var/lib/floodlight 
chmod 777 /var/lib/floodlight



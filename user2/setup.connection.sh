#!/bin/bash

if [[ -z $1 ]]; then 
    echo " Usage: $0 <controller-ip>" 
    exit 1
fi

ovs-vsctl add-br user2
ovs-vsctl set-controller user2 tcp:$1:6653

ifconfig eth1 0
ifconfig eth2 0
ifconfig user2 10.130.127.5 netmask 255.255.255.0 up 

ovs-vsctl add-port user2 eth1
ovs-vsctl add-port user2 eth2




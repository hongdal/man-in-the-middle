This repo is used for demonstrating the Man-In-The-Middle attack in SDN environment. 

The experimental environment is based on CloudLab (https://www.cloudlab.us/)

If you want to use the matirials for preparing your experiments, you need to have a CloudLab account. 

Then you can instantialte your experiment on CloudLab through this profile: 
https://www.cloudlab.us/p/SeNFV/Man-In-The-Middle-Attack/4


Visit the wiki for more instructions about the experiments:
https://bitbucket.org/hongdal/man-in-the-middle/wiki/Home

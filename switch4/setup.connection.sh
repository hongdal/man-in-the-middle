#!/bin/bash

if [[ -z $1 ]]; then 
    echo " Usage: $0 <controller-ip>" 
    exit 1
fi

ovs-vsctl add-br switch4
ovs-vsctl set-controller switch4 tcp:$1:6653

ifconfig eth1 0
ifconfig eth2 0
ifconfig switch4 10.130.127.3 netmask 255.255.255.0 up 

ovs-vsctl add-port switch4 eth1
ovs-vsctl add-port switch4 eth2




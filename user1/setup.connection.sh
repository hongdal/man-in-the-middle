#!/bin/bash

if [[ -z $1 ]]; then 
    echo " Usage: $0 <controller-ip>" 
    exit 1
fi

ovs-vsctl add-br user1
ovs-vsctl set-controller user1 tcp:$1:6653

ifconfig eth1 0
ifconfig eth2 0
ifconfig user1 10.130.127.2 netmask 255.255.255.0 up 

ovs-vsctl add-port user1 eth1
ovs-vsctl add-port user1 eth2




